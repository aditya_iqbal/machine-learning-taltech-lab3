# pylint: disable-msg=E0611

import numpy as np
import pandas as pd
import sys

from common import describe_data, test_env, classification_metrics
import common.describe_data as describe_data
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.compose import (make_column_transformer)
from sklearn.preprocessing import OneHotEncoder
from typing import (Tuple, List)
from common.describe_data import (
    print_categorical, print_overview, print_nan_counts)
from pandas import (DataFrame, read_excel, get_dummies)
from numpy import (where, ndarray, insert)
from sklearn.preprocessing import OneHotEncoder
from sklearn.compose import (make_column_transformer)
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import MultinomialNB
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from matplotlib import pyplot as plt


def read_data(file):
    """Return pandas dataFrame read from Excel file"""
    try:
        return pd.read_excel(file)
    except FileNotFoundError:
        sys.exit('ERROR: ' + file + ' not found')


def read_dataset(xlsx_path: str = "./data/students.xlsx") -> DataFrame:
    return read_excel(xlsx_path)


def preprocess_data(df: DataFrame, verbose: bool = False) -> Tuple[DataFrame, ndarray]:

    y_column = 'In university after 4 semesters'

    # Features can be excluded by adding column name to list
    drop_columns = []

    categorical_columns = [
        'Faculty',
        'Paid tuition',
        'Study load',
        'Previous school level',
        'Previous school study language',
        'Recognition',
        'Study language',
        'Foreign student'
    ]

    if verbose:
        print('Missing y values: ', df[y_column].isna().sum())

    y: ndarray = df[y_column].values
    y = where(y == 'No', 0, y)
    y = where(y == 'Yes', 1, y)
    y = y.astype(int)

    # remove the dependent variable column from the dataframe
    drop_columns.append(y_column)
    df = df.drop(labels=drop_columns, axis=1)

    # filter out the dropped columned from the categorical columns.
    categorical_columns = [
        i for i in categorical_columns if i not in drop_columns]

    # now we encode the rest of the categorical features
    for column in categorical_columns:
        df[column] = df[column].fillna(value="Missing")

    df = get_dummies(df, columns=categorical_columns, drop_first=True)
    df = df.fillna(0)

    if verbose:
        print_nan_counts(df)

    if verbose:
        print_nan_counts(df)

    return df, y


def log_reg(X, y):
    sc = StandardScaler()
    X = DataFrame(sc.fit_transform(X))
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.25, random_state=42)
    clf = LogisticRegression(random_state=0, solver='saga')
    clf.fit(X_train, y_train)
    y_pred = clf.predict(X_test)
    classification_metrics.print_metrics(
        y_test, y_pred, "Logistic Regression", 1)
    return clf


def knn(X, y):
    sc = StandardScaler()
    X = DataFrame(sc.fit_transform(X))
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.25, random_state=42)
    knn_clf = KNeighborsClassifier(n_neighbors=2).fit(X_train, y_train)
    y_pred = knn_clf.predict(X_test)
    classification_metrics.print_metrics(y_test, y_pred, "KNN", 1)
    return knn_clf


def naive_bayes(X, y):
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.25, random_state=42)
    nb = MultinomialNB().fit(X_train, y_train)
    y_pred = nb.predict(X_test)
    classification_metrics.print_metrics(
        y_test, y_pred, "Naive Bayes Classifier - Multinomial", 1)
    return nb


def svc(X, y):
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.25, random_state=42)
    svc_clf = SVC(gamma=0.001, kernel="sigmoid").fit(X_train, y_train)
    y_pred = svc_clf.predict(X_test)
    classification_metrics.print_metrics(y_test, y_pred, "SVC", 1)
    return svc_clf


def dec_tree(X, y):
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.25, random_state=42)
    dec_tree = DecisionTreeClassifier()
    dec_tree = dec_tree.fit(X_train, y_train)
    y_pred = dec_tree.predict(X_test)
    classification_metrics.print_metrics(
        y_test, y_pred, "Decision Tree Classifier", 1)
    return dec_tree


def rand_for(X, y):
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.25, random_state=42)
    rand_for = RandomForestClassifier(n_estimators=10)
    rand_for = rand_for.fit(X_train, y_train)
    
    y_pred = rand_for.predict(X_test)
    classification_metrics.print_metrics(
        y_test, y_pred, "Random Forest Classifier", 1)
    return rand_for


if __name__ == '__main__':
    modules = ['numpy', 'pandas', 'sklearn']
    test_env.versions(modules)
    students = read_data('data/students.xlsx')

    # STUDENT SHALL CALL PRINT_OVERVIEW AND PRINT_CATEGORICAL FUNCTIONS WITH
    # FILE NAME AS ARGUMENT

    students_X, students_y = preprocess_data(students)

    # knn(students_X, students_y)

    # describe data
    describe_data.print_overview(
        students, file='results/students_overview.txt')
    describe_data.print_categorical(
        students, file='results/students_categorical_data.txt')

    # STUDENT SHALL CALL CREATED CLASSIFIERS FUNCTIONS
    log_reg = log_reg(students_X, students_y)
    knn(students_X, students_y)
    naive_bayes(students_X, students_y)
    svc = svc(students_X, students_y)
    dec_tree = dec_tree(students_X, students_y)
    rand_for = rand_for(students_X, students_y)


    # get importance
    lr_importance = log_reg.coef_[0]

    plt.barh(students_X.columns, lr_importance)
    plt.xlabel("Logistic Regression Feature Importance")
    plt.show()
    # summarize feature importance
    # for i,v in enumerate(lr_importance):
	#     print('Feature: %0d, Score: %.5f' % (i,v))
    # plot feature importance
    # plt.bar([x for x in range(len(lr_importance))], lr_importance)
    # plt.show()


    # plt.barh(students_X.columns, svc.feature_importances_)
    # plt.xlabel("SVC Feature Importance")
    # plt.show()

    plt.barh(students_X.columns, dec_tree.feature_importances_)
    plt.xlabel("Decision Tree Feature Importance")
    plt.show()

    plt.barh(students_X.columns, rand_for.feature_importances_)
    plt.xlabel("Random Forest Feature Importance")
    plt.show()


    #based on the accuracy score, and to be not too optimistic, I think random forest classifier is
    #the best classfier

    print('Done')
